// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCYVaVoQHzmZpHwCNhS4F9UMFGf-Uevw1s",
    authDomain: "homework-931b3.firebaseapp.com",
    databaseURL: "https://homework-931b3.firebaseio.com",
    projectId: "homework-931b3",
    storageBucket: "homework-931b3.appspot.com",
    messagingSenderId: "706718885953",
    appId: "1:706718885953:web:ea1da961a2bc92322abec0",
    measurementId: "G-5SBDJ8C96V"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
