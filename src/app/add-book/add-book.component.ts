import { NotificationService } from './../shered/services/notification.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BooksService } from '../shered/services/books.service';


@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  book: string;
  author: string;
  newBook = new FormControl('', [Validators.required]);
  newAuthor = new FormControl('', [Validators.required]);

  getBookErrorMessage() {
    return this.newBook.hasError('required') ? 'You must enter a book title' : '';
  }
  getAuthorErrorMessage() {
    return this.newAuthor.hasError('required') ? 'You must enter a author name' : ''
  }
  constructor(private bookService: BooksService, private router: Router,
    private notificationService: NotificationService) { }

  ngOnInit() {
    
  }
  addBook(){
    this.bookService.addBooks(this.book, this.author);
    this.notificationService.success("The book added successfully!");
    this.router.navigate(['/books']);
  }
}
