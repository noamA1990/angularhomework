import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../shered/interfaces/posts.Interface';
import { PostsService } from '../shered/services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
  posts$:Observable<Post[]>;


   constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.posts$ = this.postsService.getPostsData();
  }
  addPPostsToFirestore(){
    this.postsService.addToFirestore();
    
    
  }
}
