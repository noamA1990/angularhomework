import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FireStorePostsService } from '../shered/services/fireStorePosts.service';
import { NotificationService } from '../shered/services/notification.service';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css'],
})
export class PostFormComponent implements OnInit {

  newUser = new FormControl('', [Validators.required]);
  newTitle = new FormControl('', [Validators.required]);
  newBody =  new FormControl('', [Validators.required]);
  editMode: boolean = false;
  btnText = "Add post";
  key: string;
  title: string;
  body: string;
  user: string;
  message: string;

  constructor(private fireStorePostservice: FireStorePostsService, private router: Router, 
                private route: ActivatedRoute, private location: Location, 
                private notificationService: NotificationService) { }

  getTitleErrorMessage() {
    return this.newTitle.hasError('required') ? 'You must enter a post title' : '';
  }
  getBodyErrorMessage() {
    return this.newBody.hasError('required') ? 'You must enter a post body' : '';
  }
  getUserErrorMessage() {
    return this.newUser.hasError('required') ? 'You must enter a name' : '';
  }

  ngOnInit() {

    if (this.location.path().endsWith('edit-post')) {
      this.key = this.route.snapshot.params.key;
      this.editMode = true;
      this.btnText = "Update post";
      this.fireStorePostservice.getSinglePost(this.key).subscribe(
        post => {
          this.title = post.data().title;
          this.body = post.data().body;
          this.user = post.data().author;
        }
      )
    }
  }

  submit(){
    if(this.editMode){
      this.fireStorePostservice.updatePost(this.key, this.title, this.body, this.user);
      this.message = "The post updated successfully!";
      this.notificationService.success(this.message);
    }
    else{
      this.fireStorePostservice.addNewPost(this.title, this.body, this.user);
      this.message = "The post added successfully!";
      this.notificationService.success(this.message);
    }
    this.router.navigate(['/dbposts']);
  }
}
