import { PostFormComponent } from './post-form/post-form.component';
import { FireStorePostsComponent } from './fire-store-posts/fire-store-posts.component';
import { PostsComponent } from './posts/posts.component';
import { AuthorsComponent } from './authors/authors.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { EditauthorComponent } from './editauthor/editauthor.component';
import { AddBookComponent } from './add-book/add-book.component';
import { AddAuthorComponent } from './add-author/add-author.component';
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SecureInnerPagesGuard } from './shered/guards/secure-inner-pages.guard';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { AuthGuard } from './shered/guards/auth.guard';

const appRoutes: Routes =[
    { path: '', redirectTo: '/log-in', pathMatch: 'full' , canActivate: [SecureInnerPagesGuard]},
    { path: 'log-in', component: LogInComponent, canActivate: [SecureInnerPagesGuard]},
    { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
    // { path: 'dashboard', component: DashboardComponent },
    { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
    { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] },
    
    { path: 'books', children: [
        { path: '',  component: BooksComponent, canActivate: [AuthGuard] },
        { path: 'create', component: AddBookComponent, canActivate: [AuthGuard] }
        ]
    },
    { path: 'dbposts', children: [
        { path: '', component: FireStorePostsComponent, canActivate: [AuthGuard] },
        { path: 'add-post', component: PostFormComponent, canActivate: [AuthGuard] },
        { path: ':key/edit-post', component: PostFormComponent, canActivate: [AuthGuard] },
        ]
    },
    { path: 'authors', children: [
        { path: '',  component: AuthorsComponent, canActivate: [AuthGuard] },
        { path: 'create', component: AddAuthorComponent, canActivate: [AuthGuard] },
        { path: ':id/:author', component: AuthorsComponent, canActivate: [AuthGuard] },
        { path: ':id/:author/edit', component: EditauthorComponent, canActivate: [AuthGuard] },
        ]
    },
    { path: 'posts', component: PostsComponent, canActivate: [AuthGuard] },
    { path: 'not-found', component:ErrorPageComponent, data: {message: 'Page not found!'} },
    { path: '**', redirectTo: '/not-found' }
  ]
@NgModule({
    imports: [
        // RouterModule.forRoot(appRoutes, {useHash :true})
        RouterModule.forRoot(appRoutes)

    ],
    exports: [RouterModule]
})

export class AppRoutingModdule{

}