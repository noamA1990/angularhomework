import { AuthorsService } from './../shered/services/authors.service';
import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

export interface Authors{
  id: number;
  name: string;
}

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  
  id: number;
  name: string;
  authors$:Observable<any>;
  authors: any;

  constructor(private route:ActivatedRoute, private authorService:AuthorsService) { }    

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.name = this.route.snapshot.params['author'];

    if (this.id !== undefined){
      this.authorService.editAuthor(this.id, this.name);
    }
    this.authors$ = this.authorService.getAuthorsObs();
  }
}
