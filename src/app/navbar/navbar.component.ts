import { AuthService } from './../shered/services/auth.service';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  
  title: string;
  
  isHandset$: Observable<boolean> = 
    this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    constructor(private breakpointObserver: BreakpointObserver, location: Location, router: Router,
      public authService: AuthService) {
      router.events.subscribe(val => {
        if (location.path() == "/books") {
          this.title = 'Books list';
        }
        else if (location.path() == "/books/create") {
          this.title = 'Add new book';
        }
        else if (location.path() == "/authors/create") {
          this.title = 'Add new author';
        }
        else if (location.path().endsWith('edit')) {
          this.title = "Edit author";
        }
        else if (location.path() == "/posts") {
          this.title = 'Posts List';
        }
        else if (location.path() == "/dbposts") {
          this.title = 'Database Posts List';
        }
        else if (location.path().endsWith('add-post')) {
          this.title = "Add new post";
        }
        else if (location.path().endsWith('edit-post')) {
          this.title = "Edit post";
        }
        else if (location.path() == "/authors") {
          this.title = 'Authors list';
        }
        else {
          this.title = "angularHomeWork";
        }
      });   
    }
    SignOut(){
    this.authService.SignOut();
  }
}