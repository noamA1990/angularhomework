import { FlexLayoutModule } from '@angular/flex-layout';
import { AuthService } from './shered/services/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { AuthorsComponent } from './authors/authors.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AppRoutingModdule } from './app-routing.module';
import { EditauthorComponent } from './editauthor/editauthor.component';
import {MatInputModule, MatSnackBarModule, MatDialogModule, MatDialog, MatCardModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { AddBookComponent } from './add-book/add-book.component';
import { AddAuthorComponent } from './add-author/add-author.component';
import { PostsComponent } from './posts/posts.component';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { FireStorePostsComponent, DeleteDialog } from './fire-store-posts/fire-store-posts.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { AuthorsService } from './shered/services/authors.service';
import { BooksService } from './shered/services/books.service';
import { PostsService } from './shered/services/posts.service';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { TempNavComponent } from './temp-nav/temp-nav.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BooksComponent,
    AuthorsComponent,
    ErrorPageComponent,
    EditauthorComponent,
    AddBookComponent,
    AddAuthorComponent,
    PostsComponent,
    FireStorePostsComponent,
    PostFormComponent,
    DeleteDialog,
    ForgotPasswordComponent,
    LogInComponent,
    SignUpComponent,
    VerifyEmailComponent,
    TempNavComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    AppRoutingModdule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    MatFormFieldModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    MatSnackBarModule,
    MatDialogModule,
    AngularFireAuthModule,
    FlexLayoutModule,
  ],
  providers: [AuthorsService, BooksService, PostsService,MatDialog, AuthService],
  bootstrap: [AppComponent],
  entryComponents: [DeleteDialog]

})
export class AppModule { }
