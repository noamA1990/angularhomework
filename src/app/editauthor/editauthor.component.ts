import { NotificationService } from './../shered/services/notification.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {

  authorEdit:string;
  id: number;
  author = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.author.hasError('required') ? 'You must enter a author' : '';
  }
  constructor(private router: Router, private route:ActivatedRoute,
    private notificationService: NotificationService) { }

  ngOnInit() {
    this.authorEdit = this.route.snapshot.params['author'];
    this.id = this.route.snapshot.params['id'];
  }
  
  onSubmit(){
    this.router.navigate(['/authors', this.id, this.authorEdit]);
    this.notificationService.success("The author name updated successfully!");
  }
}
