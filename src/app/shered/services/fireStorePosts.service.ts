import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Post } from '../interfaces/posts.Interface';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class FireStorePostsService{

    private itemCollection: AngularFirestoreCollection<Post>;
    private postsCollection: Observable<Post[]>;
    constructor(private db: AngularFirestore){
        this.itemCollection = db.collection<Post>('posts');
    }

    getPostsFromDB():Observable<Post[]>{
        this.postsCollection = this.itemCollection.valueChanges({idField: 'key'});
        return this.postsCollection;
    }

    addNewPost(title: string, body: string, userName: string){
        this.db.collection('posts').add({
          title: title,
          body: body,
          author: userName  
        })
    }

    getSinglePost(key:string){
        return this.db.collection('posts').doc(key).get();
    }

    updatePost(key:string, title: string, body: string, userName: string){
        this.db.collection('posts').doc(key).update({
            title: title,
            body:body,
            author: userName
        })
    }

    deletePost(key: string){
        this.db.collection('posts').doc(key).delete();
    }
}