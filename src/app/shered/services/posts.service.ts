import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Post } from '../interfaces/posts.Interface';
import { Injectable } from '@angular/core';
import { User } from '../interfaces/users.interface';
import { map, catchError } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { NotificationService } from './notification.service';



@Injectable({
    providedIn: 'root'
  })
  
export class PostsService{
    private POSTURL = "https://jsonplaceholder.typicode.com/posts";
    private USERSURL = "https://jsonplaceholder.typicode.com/users";
    private posts: Observable<Post[]>;
    private itemsCollection: AngularFirestoreCollection<Post>;

    constructor(private http: HttpClient, private db: AngularFirestore,
        private notificationService: NotificationService){
        this.itemsCollection = db.collection<Post>('posts');
    }

    getAllUsers(): Observable<User[]>{
        const users = this.http.get<User[]>(`${this.USERSURL}`);
        return users;
    }
    getPostsData(): Observable<Post[]>{
        this.posts = this.http.get<Post[]>(`${this.POSTURL}`).pipe(
            map((data) => 
            this.addUsersToPosts(data)),
            catchError(this.handleError));
        return this.posts;
    }

    private handleError(res: HttpErrorResponse) {
        console.log(res.error);
        return throwError(res.error);
      }

    addUsersToPosts(data: Post[]): Post[]{
        const users = this.getAllUsers();
        const postsArray = []
        users.forEach(user => {    
            user.forEach(u =>{
                data.forEach(post =>{
                    if(post.userId === u.id){
                        postsArray.push({
                            id: post.id,
                            userId:post.userId,
                            title: post.title,
                            body: post.body,
                            userName: u.name
                        })
                    }
                })
            })
        })
    return postsArray;
    }

    addToFirestore(){
        const users = this.getAllUsers();
        const posts = this.http.get<Post[]>(`${this.POSTURL}`);
        let items: Observable<Post[]>;
        items = this.itemsCollection.valueChanges();

        items.forEach(data => {
            if(data.length === 0){
                posts.forEach(post =>{
                    post.forEach(postEl => {
                        users.forEach(user => {
                            user.forEach(el => {
                                if(el.id === postEl.userId){        
                                    postEl.userName = el.name;
                                    this.db.collection("posts").add({
                                        title: postEl.title,
                                        body: postEl.body,
                                        author: postEl.userName
                                    })
                                }
                            })
                        })
                    })
                })
            this.notificationService.success("The posts added to db successfully!!");
            }
            else{
                this.notificationService.info("You've already added to db these posts!");
                // console.log("all posts were added!")
            }
        })
    }
}