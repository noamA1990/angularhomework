

export class BooksService{
    booksArray = [
        {title: 'Harry Potter and the Sorcerer\'s Stone (Harry Potter, #1)',author: 'J.K. Rowling'},
        {title: 'Pride and Prejudice',author: 'Jane Austen'},
        {title: 'The Great Gatsby',author: 'F. Scott Fitzgerald'},
        {title: 'Jane Eyre',author: 'Charlotte Brontë'}
    ];

    getBooks(){
        return this.booksArray;
    }

    addBooks(title: string, author: string){
        this.booksArray.push(
            {title: title, author: author }
        );
    }
}