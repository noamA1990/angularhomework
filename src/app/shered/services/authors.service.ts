import { Observable } from 'rxjs';

export class AuthorsService{
    authorsArray = [
        {id: 1, name: 'J.K. Rowling'},
        {id: 2, name: 'Jane Austen'},
        {id: 3, name: 'F. Scott Fitzgerald'},
        {id: 4, name: 'Charlotte Brontë'}
    ];
 
    getAuthors(){
        return this.authorsArray;
    }
    getAuthorsObs(): any {
        const authorsObservable = new Observable(observer => {
               setInterval(() => 
                   observer.next(this.authorsArray)
               ,100);
        });
        return authorsObservable;
    }
    editAuthor(id: number, name: string){
        this.authorsArray.forEach(author => {
            if(author.id == id){ 
                author.name = name;
            }
        });
    }

    addAtuhors(name){
        this.authorsArray.push({id: this.authorsArray.length+1, name: name});
    }
}