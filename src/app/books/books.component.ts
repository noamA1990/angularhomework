import { BooksService } from './../shered/services/books.service';
import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  
  panelOpenState = false;
  books: any;
  constructor(private bookService: BooksService) { }

  ngOnInit() {
    this.books = this.bookService.getBooks();
  }

}
