import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition,
        MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Post } from '../shered/interfaces/posts.Interface';
import { FireStorePostsService } from '../shered/services/fireStorePosts.service';
import { NotificationService } from '../shered/services/notification.service';


export interface DialogData {
  title: string;
  key: any;
}

@Component({
  selector: 'app-fire-store-posts',
  templateUrl: './fire-store-posts.component.html',
  styleUrls: ['./fire-store-posts.component.css'],
})
export class FireStorePostsComponent implements OnInit {

  panelOpenState = false;
  dbPosts$:Observable<Post[]>;
  message: string;
  duration: number;
  verticalPosition: MatSnackBarVerticalPosition;
  horizontalPosition: MatSnackBarHorizontalPosition;
  key;
  title:string;
  
  constructor(private fspService: FireStorePostsService, 
              private notificationService: NotificationService,
              private dialog: MatDialog) { }
  
  delete(title: string, key:any){
    this.title = title;
    this.key = key;
    this.openDialog();
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DeleteDialog, {
      width: '350px',
      height: '250px',
      panelClass: 'delete-dialog-container',
      disableClose:true,
      data: {title:this.title, key:this.key}
    });
            
    dialogRef.afterClosed().subscribe(result => {
      if(result !== undefined){
        this.fspService.deletePost(result);
        this.message = "The post deleted successfully!";
        this.notificationService.warn(this.message);
      }
    });
    
  }
  ngOnInit() {
    this.dbPosts$ = this.fspService.getPostsFromDB();
  }
}

@Component({
  selector: 'delete-dialog',
  templateUrl: 'delete-dialog.html',
  styleUrls: ['./fire-store-posts.component.css'],
})
export class DeleteDialog {
 
  constructor(
    public dialogRef: MatDialogRef<DeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}